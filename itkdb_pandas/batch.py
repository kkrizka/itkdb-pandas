import pandas as pd

def getBatch(c, project, batch, batchType):
    """
    Get batch contents using the `getBatchByNumber` API.

    Parameters
    ----------
    c : itkdb.Client
        The client object.
    project : str
        The project code.
    batch : str
        The batch number (ie: iPPC_BHM).
    batchType : str
        The batch type (ie: MODULE_BATCH).

    Returns
    -------
    components : pd.DataFrame
        DataFrame of batch contents processed using pd.json_normalize.
    stages : pd.DataFrame
        DataFrame of batch contents stage history processed using pd.json_normalize.

    """
    # Query the database
    res=c.get('getBatchByNumber', json={'project':'S','number':batch,'batchType':batchType})
    components = pd.json_normalize(res, record_path='components', meta='id', meta_prefix='batch.')

    stages = pd.json_normalize(res, record_path=['components','stages'], meta=[['components','serialNumber']])
    stages['dateTime']=pd.to_datetime(stages['dateTime'])

    return components, stages